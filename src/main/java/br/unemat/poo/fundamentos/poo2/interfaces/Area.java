/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package br.unemat.poo.fundamentos.poo2.interfaces;

/**
 *
 * @author air11
 */
public interface Area {
    public double calcularArea();
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package br.unemat.poo.fundamentos.poo2;

import br.unemat.poo.fundamentos.poo2.views.Principal;

/**
 *
 * @author air11
 */
public class Start {

    public static void main(String[] args) {
        
        Principal principal = new Principal();
        principal.setLocationRelativeTo(null);
        principal.setVisible(true);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.unemat.poo.fundamentos.poo2.services;

import br.unemat.poo.fundamentos.poo2.constantes.Matematicas;
import br.unemat.poo.fundamentos.poo2.interfaces.Area;

/**
 *
 * @author air11
 */
public class AreaCirculo implements Area {
    
    public double calcularArea(double raio) {        
        return Matematicas.PI * (raio*raio);        
    }    

    @Override
    public double calcularArea() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package br.unemat.poo.fundamentos.poo2.constantes;

/**
 *
 * @author air11
 */
public enum FormasGeometricas {
    
    CIRCULO("CIRCULO"), 
    QUADRADO("QUADRADO"), 
    RETANGULO("RETANGULO"), 
    TRIANGULO_RETANGULO("TRIANGULO_RETANGULO");
    
    private final String text;

    private FormasGeometricas(final String text) {
        this.text = text;
    }
    
    @Override
    public String toString(){
        return text;
    }
    
}
